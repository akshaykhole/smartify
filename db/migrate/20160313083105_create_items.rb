class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :sensor_id
      t.string :value
      t.timestamps null: false
    end
  end
end
