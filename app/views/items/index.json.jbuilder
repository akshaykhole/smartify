json.array!(@items) do |item|
  json.extract! item, :id, :sensor_id, :value, :created_at, :updated_at
  json.url item_url(item, format: :json)
end
