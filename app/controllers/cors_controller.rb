class CorsController < ApplicationController

  skip_before_filter :authenticate_user

  def handle_options_request
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS, PUT, DELETE, HEAD, PATCH'
    headers['Access-Control-Allow-Headers'] = 'X-Api-Key, Origin, X-Requested-With, Content-Type, Accept'
    headers['Access-Control-Max-Age'] = '1728000'

    head(:ok) if request.request_method == "OPTIONS"
  end
end
